import './App.css';
import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';


import Login from './pages/Login';
import CredData from './pages/CredData';
import AuthDid from './pages/AuthDid';
import CredDid from './pages/CredDid';

function App() {
  return (
    <>
      <Router basename="/uat/holder">
        <div className="app">
          <Switch>

            {/* <PrivateRoute component={Home} path="/home" /> */}

            <Route component={CredDid} path="/credDid" />
            <Route component={AuthDid} path="/authDid" />
            <Route component={CredData} path="/credData" />

            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              <Redirect to="/credData" />
            </Route>
          </Switch>
        </div>

      </Router>
    </>
  );
}

export default App;
