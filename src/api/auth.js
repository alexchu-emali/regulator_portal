import axios from 'axios';
import jwt from 'jsonwebtoken';

const OAUTH_SERVER = 'https://corpidpoc.com.hk/uat/auth';

export const login = async (username, password) => {
    try {
        let response = await axios({
            method: 'post',
            url: OAUTH_SERVER + '/login',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            data: {
                channel: 'username_password',
                username: username,
                password: password
            }
        });
        if (response.data && response.data.access_token) {
            let userId = response.data.user_id;
            let access_token = response.data.access_token;
            let loginDate = new Date().getTime();
            let decoded = jwt.decode(access_token);
            if (Array.isArray(decoded.authorities) && decoded.authorities.includes("ROLE_ISSUER")) {
                localStorage.setItem('loginDate', loginDate);
                localStorage.setItem('userId', userId);
                localStorage.setItem('access_token', access_token);
                return await getUser(userId, access_token);
            }
        }
    } catch (e) {
        console.error("login error");
        console.error(e);
    }
    return false;
}

export const getUser = async (userId, access_token) => {
    try {
        let response = await axios({
            method: 'get',
            url: OAUTH_SERVER + '/user/' + userId,
            headers: {
                'Authorization': 'Bearer ' + access_token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        });
        if (response.data) {
            let data = response.data;
            console.log("auth.js");
            console.log(data);
            localStorage.setItem('userInfo', JSON.stringify(data));
            return true;
        }
    } catch (e) {
        console.error("getUser error");
        console.error(e);
    }
    return false;
}

export const logOut = () => {
    localStorage.removeItem('userInfo');
    localStorage.removeItem('access_token');
    localStorage.removeItem('userId');
    localStorage.removeItem('loginDate');
}