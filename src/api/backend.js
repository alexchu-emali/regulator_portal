import axios from 'axios';

const BACKEND_SERVER = 'https://corpidpoc.com.hk/uat/fabric/';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJvcmdhZG1pbiIsInVzZXJuYW1lIjoib3JnYWRtaW4iLCJyb2xlIjoiYWRtaW4iLCJleHAiOjE2NTIyNTk0NzcsImlhdCI6MTYyMDcyMzQ3N30.E5SoLQoxpsUhPIQkpn2yBeurQEwX_hyHUvCh8FYWZao';

export const queryCredDatas = async (data) => {
    try {
        let response = await axios({
            method: 'get',
            url: BACKEND_SERVER + 'api/poc/credentialdatas',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-TOKEN':token
            }
        });
        let body = response.data
        if (body.code === 200) {
            return body.data;
        }
    } catch (e) {
        console.error("queryCredDatas error");
        console.error(e);
    }
    return null;
}

export const queryCredDatasById = async (id) => {
    try {
        let response = await axios({
            method: 'get',
            url: BACKEND_SERVER + 'api/poc/credentialdata/info/'+id,
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-TOKEN':token
            }
        });
        let body = response.data
        if (body.code === 200) {
            return body.data;
        }
    } catch (e) {
        console.error("queryCredDatasById error");
        console.error(e);
    }
    return null;
}

export const queryAuthDID = async (data) => {
    try {
        let response = await axios({
            method: 'get',
            url: BACKEND_SERVER + 'api/poc/authenticationdids',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-TOKEN':token
            }
        });
        let body = response.data
        if (body.code === 200) {
            return body.data;
        }
    } catch (e) {
        console.error("queryAuthDID error");
        console.error(e);
    }
    return null;
}


export const queryAuthDidById = async (id) => {
    try {
        let response = await axios({
            method: 'get',
            url: BACKEND_SERVER + 'api/poc/authenticationdid/info/'+id,
            // url: BACKEND_SERVER + 'api/poc/authenticationdid/info/did:corpidpoc:fdbf8e47fa2f79a5754560f5bfaa9c7806b38c14a377fabde53a33d7c258a717',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-TOKEN':token
            }
        });
        let body = response.data
        if (body.code === 200) {
            return body.data;
        }
    } catch (e) {
        console.error("queryAuthDidById error");
        console.error(e);
    }
    return null;
}

export const queryCredDID = async (data) => {
    try {
        let response = await axios({
            method: 'get',
            url: BACKEND_SERVER + 'api/poc/credentialdids',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-TOKEN':token
            }
        });
        let body = response.data
        if (body.code === 200) {
            return body.data;
        }
    } catch (e) {
        console.error("queryCredDID error");
        console.error(e);
    }
    return null;
}


export const queryCredDidById = async (id) => {
    try {
        let response = await axios({
            method: 'get',
            url: BACKEND_SERVER + 'api/poc/credentialdid/info/'+id,
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-TOKEN':token
            }
        });
        let body = response.data
        if (body.code === 200) {
            return body.data;
        }
    } catch (e) {
        console.error("queryCredDidById error");
        console.error(e);
    }
    return null;
}