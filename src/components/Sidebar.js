import React from 'react';
import './Sidebar.css';
import { Link, NavLink,useHistory } from 'react-router-dom';

import { AiOutlineHome, AiOutlineFileText, AiOutlineImport, AiOutlineEdit } from 'react-icons/ai';
import {logOut} from '../api/auth';

function Sidebar() {
    const history = useHistory();
    function logout(){
        console.log('logout');
        history.push('/login');
    }
    return (
        <>
            <div className="sidebar-wrapper">
                <ul className={"nav-options"}>
                    <li className="head">
                        <p>Regulator Portal</p>
                    </li>
                    <li className="title">
                        Data Schema
                    </li>
                    <NavLink activeClassName="active" to="/credData">
                        <li className="option">
                            <AiOutlineFileText /><span>Credential Data</span>
                        </li>
                    </NavLink>

                    <NavLink activeClassName="active" to="/authDid">
                        <li className="option">
                            <AiOutlineFileText /><span>Authentication DID</span>
                        </li>
                    </NavLink>

                    <NavLink activeClassName="active" to="/credDid">
                        <li className="option">
                            <AiOutlineFileText /><span>Credential DID</span>
                        </li>
                    </NavLink>

                    <li className="title">
                        Setting
                    </li>
                    <Link to="/login" onClick={()=>logOut()}>
                        <li className="option">
                            <AiOutlineImport /><span>Log Out</span>
                        </li>
                    </Link>
                </ul>
            </div>

        </>
    )
}

export default Sidebar
