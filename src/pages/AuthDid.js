import React, { useMemo, useState, useEffect } from 'react';

import Sidebar from '../components/Sidebar';

import { Link } from 'react-router-dom';
import { useTable, usePagination, useSortBy, useExpanded } from 'react-table';
import { AiOutlinePlus, AiOutlineVerticalRight, AiOutlineVerticalLeft, AiOutlineRight, AiOutlineLeft, AiOutlineArrowDown, AiOutlineArrowUp } from "react-icons/ai";
import { IoClose } from "react-icons/io5";

import Moment from 'react-moment';
import 'moment-timezone';

import { useForm } from "react-hook-form";
import TextField from '@material-ui/core/TextField';

import { queryAuthDID,queryAuthDidById } from '../api/backend';

function AuthDid() {

    const [authDidList, setauthDidList] = useState([]);
    const { register, handleSubmit, control, errors } = useForm();

    const onSubmit = data => {
        console.log(data);
        if(data.id != ''){
            getAuthDidById(data.id);
        }else{
            fetchData();
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    async function fetchData() {
        let result = await queryAuthDID();

        if (result) {
            console.log(result)
            setauthDidList(result);
        }
    }


    async function getAuthDidById(id){
        let result = await queryAuthDidById(id);

            if (result) {
                console.log(result)
                setauthDidList([result]);
            }
    }

    let data;
    data = useMemo(() => authDidList)

    const columns = useMemo(
        () => [
            {
                Header: 'Create At',
                accessor: 'createdAt', // accessor is the "key" in the data
                Cell: ({ cell: { value } }) =>
                    <Moment date={value} format="DD MMM yyyy" />
            },
            {
                Header: 'ID',
                accessor: 'id',
                Cell: ({ cell: { value } }) => {
                    // return value.substring(14)
                    return value
                }
            },
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Type',
                accessor: 'type',
                Cell: ({ cell: { value } }) => {
                    switch (value) {
                        case 'Verifier':
                            return <div className="status green"> Verifier</div>
                        case 'Issuer':
                            return <div className="status blue"> Issuer</div>
                        case 'Corporate':
                            return <div className="status purple"> Corporate</div>
                        case 'DataProvider':
                            return <div className="status red"> Data Provider</div>
                    }
                }
            },
            {
                Header: ' ',
                id: 'expander',
                accessor: data => `${data.publicKey}` ,
                Cell: ({ cell: { row } }) => {
                    // console.log(JSON.parse(row.values.expanded));

                    // console.log(row.values.expander);
                    let action = <span {...row.getToggleRowExpandedProps()}>
                        {row.isExpanded ? <IoClose /> : <span className="view-btn">View</span>}
                    </span>
                    return (
                        action
                    )

                },
            }

        ],
        []
    )

    function Table({ columns, data, renderRowSubComponent }) {
        // Use the state and functions returned from useTable to build your UI
        const {
            getTableProps,
            getTableBodyProps,
            headerGroups,
            prepareRow,
            page, // Instead of using 'rows', we'll use page,
            // which has only the rows for the active page

            // The rest of these things are super handy, too ;)
            canPreviousPage,
            canNextPage,
            pageOptions,
            pageCount,
            gotoPage,
            nextPage,
            previousPage,
            setPageSize,
            visibleColumns,
            state: { pageIndex, pageSize, expanded },
        } = useTable(
            {
                columns,
                data,
                initialState: {
                    pageIndex: 0, sortBy: [
                        {
                            id: 'createdAt',
                            desc: true
                        }
                    ]
                },
            },
            useSortBy,
            useExpanded,
            usePagination
        )


        // render table UI
        return (
            <>
                <table {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                        <div className="tableHeader">
                                            <div>{column.render('Header')}</div>
                                            <span>
                                                {column.isSorted
                                                    ? column.isSortedDesc
                                                        ? <AiOutlineArrowDown />
                                                        : <AiOutlineArrowUp />
                                                    : ''}
                                            </span>
                                        </div>
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                    <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row)
                            return (
                                <React.Fragment {...row.getRowProps()}>
                                    <tr>
                                        {row.cells.map(cell => {
                                            return (
                                                <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                            )
                                        })}
                                    </tr>
                                    {row.isExpanded ? (
                                        <tr>
                                            <td colSpan={visibleColumns.length} style={{ padding: '0px' }}>
                                                {renderRowSubComponent({ row })}
                                            </td>
                                        </tr>
                                    ) : null}
                                </React.Fragment>
                            )
                        })}
                    </tbody>
                </table>
                {/* 
                Pagination can be built however you'd like. 
                This is just a very basic UI implementation:
                */}
                <div className="pagination">
                    <span className="pages-display">
                        Page{' '}
                        <strong>
                            {pageIndex + 1} of {pageOptions.length}
                        </strong>{' '}
                    </span>
                    <div className="pages-controller">
                        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                            {<AiOutlineVerticalRight />}
                        </button>{' '}
                        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
                            {<AiOutlineLeft />}
                        </button>{' '}
                        <button onClick={() => nextPage()} disabled={!canNextPage}>
                            {<AiOutlineRight />}
                        </button>{' '}
                        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                            {<AiOutlineVerticalLeft />}
                        </button>{' '}
                    </div>
                </div>
            </>
        )
    }

    const renderRowSubComponent = React.useCallback(
        ({ row }) => {
            console.log(row);
            let data = row.values.expander;
            console.log(data);
            return (
                <>
                    <div className="detail-wrapper">
                        <p>Public Key</p>
                        <code>
                            {data}
                        </code>
                    </div>
                </>
            )
        },
        []
    )

    return (
        <>
        <div className="container">
            <div className="container-inner">
                <Sidebar />
                <div className="main-wrapper">
                    <div className="head-wrapper">
                        <div className="title">Authentication DID</div>
                    </div>

                    <div className="blank-wrapper authDID-table-wrapper">
                    <form className="search-form">
                                <div className="search-input">
                                <TextField
                                        // required
                                        // error={errors.id ? true : false}
                                        variant="outlined"
                                        margin="normal"
                                        size="small"
                                        inputRef={register({
                                            // required: "id is required",
                                        })}
                                        fullWidth
                                        id="id"
                                        label="Serach data by ID"
                                        name="id"
                                        // helperText={errors.id?.message}
                                />
                                </div>
                               
                                <div className="btn-wrapper">
                                    <div className="btn" onClick={handleSubmit(onSubmit)}>Submit</div>
                                </div>
                            </form>

                        {authDidList.length > 0 ? <Table columns={columns} data={data} renderRowSubComponent={renderRowSubComponent} /> : <div>No data yet</div>}

                    </div>
                </div>

            </div>
        </div>
    </>
    )
}

export default AuthDid
